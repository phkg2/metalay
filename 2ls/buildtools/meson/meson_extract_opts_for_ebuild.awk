#
# Parse a meson_options.txt file and spit out ebuild fragments

BEGIN {
	# Break records at 'option(' boundary, don't parse fields apart.
	RS="[=[:space:]]+option\\(";
	FS="";
	ORS="";

	# If no prefix is given, don't add a prefix char before the name
	# If prefix or suffix chars are not specified, they default to '_'.
	# If they are specified, only '-' and '_' are treated as valid.
	# Any invalid value is reduced to no character.
	if (!useprefix) {
		PREFIXCHAR="";
	} else if (!prefixchar) {
		PREFIXCHAR="_";
	} else {
		gsub(/[^-_]/, "", prefixchar);
		PREFIXCHAR=prefixchar;
	};

	if (!suffixchar) {
		SUFFIXCHAR="_";
	} else {
		gsub(/^[-_]/, "", suffixchar);
		SUFFIXCHAR=suffixchar;
	};

	# Parse comma separated strings into arrays for use* options.

	# Don't prefix the use flag for these option
	if (useraw != "" ) {
		n=split(useraw,aut,",");
		for(i=1;i<=n;i++) {
			ut=aut[i];
			auseraw[ut]=ut;
		};
	};

	# Use alternate names for use flags for these option
	if (userename != "" ) {
		n=split(userename,aut,",");
		for(i=1;i<=n;i++) {
			# Split renames at the ':' char -- orig:new
			split(aut[i],aaut,":");
			ut=aaut[1];
			auserename[ut]=aaut[2];
		};
	};

	# Don't generate use flags/defines for these options
	if (ignore != "" ) {
		n=split(ignore,il,",");
		for(i=1;i<=n;i++) {
			it=il[i];
			aignore[it]=it;
		};
	};

	# Force default to +enable/-disable flags for these options
	if (usedefault != "" ) {
		n=split(usedefault,aut,",");
		for(i=1;i<=n;i++) {
			ut=substr(aut[i],2);
			utt=substr(aut[i],1,1)
			if ( utt == "+" ) {
				ausedefaultenable[ut]=ut;
			} else if ( utt == "-" ) {
				ausedefaultdisable[ut]=ut;
			}
		};

	};


};

# Parse rows with at least one record
NR > 1 {
	parens=1;
	start=1;
	for ( c=1; c <= NF; c++) {
		if( $c  == "(" ) { ++parens; };
		if ( $c == ")" ) { end=c-1 ; --parens;};
		if(parens==0) {
			o=substr($0,start,end);
			# Normalize spacing
			gsub(/\n/," ", o);
			gsub(/[[:space:]]+/," ", o);
			gsub(/^[[:space:]]+/, "", o);
			gsub(/[[:space:]]+$/, "", o);

			# Split option definition into optname and od at the first comma
			comma=index(o,",");
			optname=substr(o,2,comma-3);

			od=substr(o,comma+1);
			gsub(/^[[:space:]]/, "", od);

			# Capture option name to opts array and data to optdata array
			opts[optname]=optname;
			optdata[optname]=od;
			break;
		};
	};
	$0=$0;
};


END {

	# Iterate over list of option names
	for(o in opts) {
		# Grab option data for this option
		od=optdata[o];

		# Parse apart tagged data format
		while(od != "") {
			comma=0;
			colon=0;
			bracket=0;
			arr="";
			val="";

			# Find first tag remaining in list
			colon=index(od,":");
			tag=substr(od,1,colon-1);
			gsub(/^[[:space:]]+/, "", tag);
			gsub(/[[:space:]]+$/, "", tag);
	
			# Strip off tag before and extra spaces after ':'
			od=substr(od,colon+1);
			gsub(/^[[:space:]]+/, "", od);

			# Detect special characters
			comma=index(od,",");
			quote=index(od,"'");
			bracket=index(od,"[");

			# Handle lists in brackets
			if( ( comma > bracket ) && match(od,"\\[[^\\]]*\\]") ) {
				arr=substr(od,RSTART+1,RLENGTH-2);
				od=substr(od,RSTART+RLENGTH);
				gsub(/[[:space:]]+$/, "", od);
				comma=index(od,",");
				val=arr;
			# Handle quoted strings
			} else if ( (quote > 0 ) && ((comma > quote) || (comma < 1) ) ) {
				for(t=quote+1; t<=length(od); t++ ) {
					if( ( substr(od,t,1) == "'" ) && ( substr(od,t-1,1) != "\\" ) ) {
						val=substr(od,quote+1,(t-quote)-1);
						od=substr(od,t+1);
						gsub(/[[:space:]]+$/, "", od);
						comma=index(od,",");
						break;
					}
				}
			}

			# Split tags at commas or end of record.
			if(comma > 0) {
				if (val == "" ) { val=substr(od,1,comma-1); }
				od=substr(od,comma + 1);
			} else {
				if (val == "" ) { val=od; };
				od="";
			}

			# Trim trailing spaces
			gsub(/[[:space:]]+$/, "", od);
			gsub(/[[:space:]]+$/, "", val);

			# Add this value to tag array for this option name
			# (gawk would make this cleaner with proper multidim arrays and indirect function calls)
			if( tag == "type" ) {
				type[o]=val;
			};
			if( tag == "choices" ) {
				choices[o]=val;
			};
			if( tag == "value" ) {
				value[o]=val;
			};
			if( tag == "min" ) {
				min[o]=val;
			};
			if( tag == "max" ) {
				max[o]=val;
			};
			if( tag == "description" ) {
				description[o]=val;
			};

		};

		# Fill additional arrays with data for this option
		efftype[o]=get_efftype(type[o], choices[o]);

		# Dump out comments for this option definition.
		printf("\n# Option: %s\n", o);
		printf("#\ttype: %s", type[o]);
		if (efftype[o] != type[o] ) {
			printf(" (treating as %s)\n", efftype[o]);
		} else {
			printf("\n");
		};
		if (description[o] != "") { printf("#\tdescription: %s\n", description[o] ); };
		if (choices[o] != "") { printf("#\tchoices: %s\n", choices[o]); };
		if (value[o] != "") { printf("#\tvalue: %s\n", value[o]); };
		if (min[o] != "") { printf("#\tmininum value: %s\n", min[o]); };
		if (max[o] != "") { printf("#\tmaximum value: %s\n", max[o]); };

		# Unless on the ignore list,
		if ( aignore[o] != "" ) {
			printf("# IGNORED (will use build system defaults)\n");
		# dump out use flag or string definitons for this option and setup config opts.
		} else {
			printf("%s", meson_ebuild_use(o));
			printf("%s", meson_ebuild_define(o));
			config_opts=config_opts meson_ebuild_conf_opts(o);
		}
	};

	printf("\nIUSE=\"${IUSE}%s\"\n", iuselist);
	printf("\nset_mesonargs() {\n\temesonargs=(\n%s\t)\n}\n\n", config_opts );

};



# Uppercase and convert dashes to underscores for shell var names
function _UC_(mytext,   mytmp) {
	mytmp=mytext;
	gsub(/-/,"_", mytmp);
	mytmp=toupper(mytmp);
	return(mytmp);
};

# Get use flag name for option
function get_usename(optname, choice,   ubtmp) {
	ubtmp=""

	# If option name isn't in the auseraw array, prefix it with the current useprefix
	if (auseraw[optname] != optname ) {
		ubtmp=ubtmp useprefix PREFIXCHAR;
	};
	ubtmp=ubtmp optname;

	# Explicit use rename overrides prefixing
	if (auserename[optname] != "" ) {
		ubtmp=auserename[optname];
	};

	# If choice passed, add as suffix
	if (choice != "" ) {
		ubtmp=ubtmp SUFFIXCHAR choice;

		if (auserename[optnam SUFFIXCHAR choice] != "" ) {
			ubtmp=auserename[optname SUFFIXCHAR choice];
		};
	};
	return(ubtmp);
};


# Get effective type for option based on stated type and choices
function get_efftype(opttype, optchoices) {

	# Handle boolean (including combo boolean with auto option)
	if(opttype=="boolean" || (optchoices ~ /auto/ && optchoices ~ /true/ && optchoices ~ /false/)) {
		return("boolean");
	# Handle features (including combos that look like features)
	} else if(opttype=="feature" || (opttype=="combo" && optchoices ~ /auto/ && optchoices ~ /enabled/ && optchoices ~ /disabled/)) {
		return("feature");
	# Handle normal arrays and combos
	} else if (opttype=="array" || opttype=="combo") {
		return(opttype);
	# Handle strings and integers
	} else if (opttype=="string" || opttype=="integer") {
		return(opttype);
	# Unknown meson option type
	} else {
		return("unknown");
	};
};


# Given the name of an option, return the IUSE_XX var line for it, add that var to the iuse list, and note any 'none' type options in usenone[].
# Must be run after arrays indexed by opt have been generated, but before functions which need to know about usenone options
function meson_ebuild_use(opt,    ret,oiu,ocl, myoptchoicenum,myoptchoices,nc,i,myoptchoice, myoptvalsnum,myoptvals,j,myoptval) {
	ret="";
	ocl=""
	# Booleans and features are switched by their use flag base named
	if(efftype[opt]=="boolean" || efftype[opt]=="feature" ) {
		myun=get_usename(opt);
		if(efftype[opt]=="boolean" ) {
			myenval="true";
		} else if(efftype[opt]=="feature" ) {
			myenval="enabled";
		};

		if( !ausedefaultdisable[myun] && (ausedefaultenable[myun] != "" || value[opt]==myenval) ) {mydef="+"; } else {mydef=""};
		ret="IUSE_" _UC_(opt) "=\"" mydef myun "\"\n";
		iuselist=iuselist " ${IUSE_" _UC_(opt) "}" ;
	# For arrays and combos, individual choices are suffixed to the option base names.
	} else if (type[opt]=="array" || type[opt]=="combo") {
		myoptchoicenum=split(choices[opt],myoptchoices,",");
		# Setup our output variable
		# Add to this IUSE_x variable to 'iuselist' global for later use
		iuselist=iuselist " ${IUSE_" _UC_(opt) "}" ;
		nc=0;
		for (i=1;i<=myoptchoicenum;i++) {
			myoptchoice=myoptchoices[i];
			gsub(/['[:space:]]/,"",myoptchoice);
			# If we have this choice for this option on the ignore list, skip
			if ( aignore[ opt SUFFIXCHAR myoptchoice] ) { continue; };
			# Don't add flags for empty or auto choices
			if ( myoptchoice == "" || myoptchoice == "auto" ) { continue; };
			# Handle 'none' type choices, then skip remainder
			if ( myoptchoice == "disabled" || myoptchoice == "none" ) {
				useoptnone[opt]=myoptchoice;
				continue;
			};

			# Add spaces between output entries
			if (++nc>1) { oiu=oiu " ";  ocl=ocl " ";};

			myunc=get_usename(opt,myoptchoice);

			# Add + to IUSE flags to reflect option value
			myoptvalsnum=split(value[opt],myoptvals,",");
			for(j=1;j<=myoptvalsnum;j++) {
				myoptval=myoptvals[j];
				gsub(/['[:space:]]/,"",myoptval);
				if ( myoptchoice == "") { continue; }
				if( !ausedefaultdisable[myunc] && (ausedefaultenable[myunc] != "" || myoptchoice == myoptval) ) {
					oiu=oiu "+"; break;
				};
			}

			# Add the use flag to IUSE_x and choice to CHOICES_x
			oiu=oiu myunc;
			ocl=ocl myoptchoice;
		};
		# As long as we had at least one valid choice, return IUSE and CHOICES lines
		if (nc > 0) {
			ret="IUSE_" _UC_(opt) "=\"" oiu "\"\n";
			ret=ret "CHOICES_" _UC_(opt) "=\"" ocl "\"\n";
		};

	} else {
	};

	return ret;

};

function meson_ebuild_conf_opts(opt,   optn, ret) {
	ret="";

	if(aignore[opt]!="" ) {
		ret="\t\t#Ignored option" opt ".\n";
	} else if(efftype[opt]=="boolean" ) {
		ret="\t\t$(" "meson_option_boolean " opt " " get_usename(opt) ")\n";
	} else if(efftype[opt]=="feature" ) {
		ret="\t\t$(" "meson_option_feature " opt " " get_usename(opt) ")\n";
	} else if (type[opt]=="array" || type[opt]=="combo") {
		if ( useoptnone[opt] != "" ) {
			ret="\t\t$(" "meson_option_none_or_" type[opt] " " opt " \"" useoptnone[opt] "\"" " \"${IUSE_" _UC_(opt) "}\" \"${CHOICES_" _UC_(opt) "}\")\n";
		} else {
			ret="\t\t$(" "meson_option_" type[opt] " " opt " \"${IUSE_" _UC_(opt) "}\" \"${CHOICES_" _UC_(opt) "}\")\n";
		};
	} else if (type[opt]=="string") {
		ret="\t\t$(" "meson_option_string " opt " \"${OPT_" _UC_(opt) "}\")\n";
	} else if (type[opt]=="integer") {
		ret="\t\t$(" "meson_option_integer " opt " \"${OPT_" _UC_(opt) "}\")\n";
	};
	return(ret);
}

# Define a string or integer variable
function meson_ebuild_define(opt,   ret) {
	ret=""
	if( aignore[opt] == "" && ( type[opt]=="string" || type[opt]=="integer") ) {
		ret=": ${OPT_" _UC_(opt) ":=\"" value[opt] "\"}\n";
	} else {
	};
	return ret;
};
