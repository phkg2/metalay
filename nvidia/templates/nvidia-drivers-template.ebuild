# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
inherit eutils multilib-minimal

DESCRIPTION="NVIDIA Accelerated Graphics Driver"
HOMEPAGE="http://www.nvidia.com/ http://www.nvidia.com/Download/Find.aspx"

AMD64_NV_PACKAGE="NVIDIA-Linux-x86_64-${PV}"
ARM_NV_PACKAGE="NVIDIA-Linux-armv7l-gnueabihf-${PV}"
X86_NV_PACKAGE="NVIDIA-Linux-x86-${PV}"

AMD64_FBSD_NV_PACKAGE="NVIDIA-FreeBSD-x86_64-${PV}"
X86_FBSD_NV_PACKAGE="NVIDIA-FreeBSD-x86-${PV}"

NV_URI="http://download.nvidia.com/XFree86/"

SRC_URI="
	amd64? (
		compat32? ( ${NV_URI}Linux-x86_64/${PV}/${AMD64_NV_PACKAGE}.run )
		!compat32? ( ${NV_URI}Linux-x86_64/${PV}/${AMD64_NV_PACKAGE}-no-compat32.run )
	)
	amd64-fbsd? ( ${NV_URI}FreeBSD-x86_64/${PV}/${AMD64_FBSD_NV_PACKAGE}.tar.gz )
"
PVMAJ="${PV%%.*}"

if [ ${PVMAJ} -le 390 ] ; then
	SRC_URI="${SRC_URI}
		x86? ( ${NV_URI}Linux-x86/${PV}/${X86_NV_PACKAGE}.run )
		x86-fbsd? ( ${NV_URI}FreeBSD-x86/${PV}/${X86_FBSD_NV_PACKAGE}.tar.gz )
		arm? ( ${NV_URI}Linux-32bit-ARM/${PV}/${ARM_NV_PACKAGE}.run )
	"
fi

LICENSE="GPL-2 NVIDIA-r2"
SLOT="0/${PV%.*}"
KEYWORDS="-* ~amd64 ~amd64-fbsd"
RESTRICT="bindist mirror strip"
EMULTILIB_PKG="true"

# Flags for desktop environments support
IUSE_DESKTOPS="+X +wayland"

# Flags to enable gpu based compute frameworks
IUSE_COMPUTE="+opencl +cuda"

# Install 32bit compatibility libraries
IUSE_COMPAT32="+compat32"

# Kernel flavor specific tweaks triggered by these flags
IUSE_KERNELS="kernel_FreeBSD kernel_linux"

# All of the following are installed in source form for use by nvidia-kernel-modules package
NV_SRC_MODS="nvidia_drm nvkms nvlink nvswitch resman uvm"


# OpenGL/EGL Subsystems
NV_MODS="${NV_MODS} egl opengl"

# OpenCL/CUDA base modules
NV_MODS="${NV_MODS} compiler gpgpu gpgpucomp"

# Video CODEC libraries
NV_MODS="${NV_MODS} encodeapi nvcuvid vdpau"

# Frame readback and capture
NV_MODS="${NV_MODS} nvfbc nvifr"

# Management later (smi)
NV_MODS="${NV_MODS} nvml"

# Fancy advanced feature libs
if [ ${PVMAJ} -ge 400 ] ; then NV_MODS="${NV_MODS} optix raytracing" ; fi
if [ ${PVMAJ} -ge 418 ] ; then NV_MODS="${NV_MODS} opticalflow optix" ; fi

# Xorg display driver (+base config)
NV_MODS="${NV_MODS} xdriver"

for mod in ${NV_MODS} ; do
	IUSE_NVMODS="${IUSE_NVMODS} +${mod}"
done
unset mod


## The following binaries can (and probably should) be built from sources

# nvidia-xconfig & nvidia-settings (+libnvidiagtk[23])
NV_BINTOOLS="${NV_BINTOOLS} xutils"

# Persistence daemon
NV_BINTOOLS="${NV_BINTOOLS} nvpd"

# Flag to enable linking binary tools into system path
IUSE_NV_BINTOOLS="bin-tools"

# Use flags for bin-tools providing modules
for mod in ${NV_BINTOOLS} ; do
	IUSE_NV_BINTOOLS="${IUSE_NV_BINTOOLS} ${mod}"
done
unset mod


# This flag being enabled is mandatory and required for supporting other packages which expect it.
IUSE="${IUSE} +glvnd"
REQUIRED_USE="glvnd"



IUSE="${IUSE} ${IUSE_DESKTOPS} ${IUSE_COMPUTE} ${IUSE_COMPAT32} ${IUSE_KERNELS} ${IUSE_NVMODS} ${IUSE_NV_BINTOOLS}"
IUSE="${IUSE} +tools"

# Handle glvnd (default since 361.16)
# Note: If you want to support 340 or earlier, or non-glvnd through 430,
#       you'll need to hack on eselect-opengl in addition to this template.

# This flag being enabled is mandatory and required for supporting other packages which expect it.
IUSE="${IUSE} +glvnd"
REQUIRED_USE="glvnd"

# Our kernel modules use the sources installed nvidia-drivers-bin
DEPEND="
	=x11-drivers/nvidia-drivers-bin-${PV}*:${SLOT}
	=x11-drivers/nvidia-kernel-modules-${PV}*:${SLOT}

	tools? (
		bin-tools? (
			=x11-drivers/nvidia-drivers-bin-${PV}*:${SLOT}[bin-tools]
		)
		!bin-tools? (
			=x11-drivers/nvidia-drivers-bin-${PV}*:${SLOT}[-bin-tools]
			kernel_linux? (
				=x11-drivers/nvidia-modprobe-${PV}*:${SLOT}
				=x11-drivers/nvidia-persistenced-${PV}*:${SLOT}
			)
			X? (
				=x11-drivers/nvidia-xconfig-${PV}*:${SLOT}
				=media-video/nvidia-settings-${PV}*:${SLOT}
			)
			
		)
	)
"


S="${WORKDIR}/"

NV_ROOT="${EPREFIX}/opt/nvidia/${P}"
NV_DISTFILES_PATH="${NV_ROOT}/distfiles/"
NV_NATIVE_LIBDIR="${NV_ROOT%/}/lib64"
NV_COMPAT32_LIBDIR="${NV_ROOT%/}/lib32"

QA_PREBUILT="${NV_ROOT#${EPREFIX}/}/*"

# Relative to $NV_ROOT
NV_BINDIR="bin"
NV_INCDIR="include"
NV_SHAREDIR="share"

# Relative to $NV_ROOT/lib{32,64}
NV_LIBDIR="/"
NV_OPENGL_VEND_DIR="opengl/nvidia"
NV_OPENCL_VEND_DIR="OpenCL/nvidia"
NV_X_MODDIR="xorg/modules"

# Maximum supported kernel version in form major.minor
: "${NV_MAX_KERNEL_VERSION:=5.0}"

pkg_setup() {
	true
}

src_unpack() {
	true
}

src_install() {
	true
}

pkg_postinst() {
	true
#	readme.gentoo_print_elog
#
#	if ! use X; then
#		elog "You have elected to not install the X.org driver. Along with"
#		elog "this the OpenGL libraries and VDPAU libraries were not"
#		elog "installed. Additionally, once the driver is loaded your card"
#		elog "and fan will run at max speed which may not be desirable."
#		elog "Use the 'nvidia-smi' init script to have your card and fan"
#		elog "speed scale appropriately."
#		elog
#	fi

}

