# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="NVIDIA drivers persistenced Utility"
HOMEPAGE="http://www.nvidia.com/"
SRC_URI="https://download.nvidia.com/XFree86/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0/${PV%.*}"
KEYWORDS="-* ~arm ~arm64 ~ppc64 ~amd64 ~x86 ~x86-fbsd"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"

PATCHES=()

src_prepare() {
	default
	sed \
		-e 's/_BSD_SOURCE/_DEFAULT_SOURCE/' \
		-e 's/-I $(RPC_DIR)/& $(RPC_INC)/' \
		-e 's/$(LIBS)/& $(RPC_LIBS)/' \
		-i Makefile
}

src_compile() {
	emake \
		AR="$(tc-getAR)" CC="$(tc-getCC)" LD="$(tc-getLD)" RANLIB="$(tc-getRANLIB)" \
		DO_STRIP= NV_VERBOSE=1 MANPAGE_GZIP=0 \
		RPC_INC="-I${EPREFIX}/usr/include/tirpc" RPC_LIBS="-ltirpc" \
		PREFIX="${EPREFIX}/usr" LIBDIR="$(get_libdir)" OUTPUTDIR="."
}

src_install() {
	emake \
		DO_STRIP= NV_VERBOSE=1 MANPAGE_GZIP=0 \
		PREFIX="${EPREFIX}/usr" LIBDIR="${ED}/usr/$(get_libdir)" OUTPUTDIR="." \
		DESTDIR="${D}" install

		# Install conf and init files
		newconfd "${FILESDIR}/nvidia-persistenced.conf" nvidia-persistenced
		
		filename="nvidia-persistenced.init"
		sed -e 's:/opt/bin:'"${EPREFIX}"'/usr/bin:g' "${FILESDIR}/${filename}" > "${T}/${filename}"
		newinitd "${T}/${filename}" "${filename%.init}"
}

