#!/bin/sh

die() {
	if [ $# -gt 0 ] ; then
		printf -- "\n%s\n" 1>&2
	else
		printf -- "${0} called die!" 1>&2
	fi
	exit 1
}



# Clone or pull, as needed
# git_clone_or_pull() <repo soure uri> <repo dest directory>
git_clone_or_pull() {
	[ $# -eq 2 ] || die "git_clone_or_pull requires 2 arguments."
	my_repo_uri="${1}"; shift
	my_repo_dir="${1}"; shift
	if [ -d "${my_repo_dir}" ] ; then
		[ -d "${my_repo_dir}/.git" ] || die "Repo destination directory '${my_repo_dir}' exists, but is not a git repo!"
		success=0
		cd "${my_repo_dir}" || die "Could not change to directory '${my_repo_dir}'."
		git checkout -f master
		git pull origin && success=1
		cd - > /dev/null
		[ ${success} -eq 1 ] || die "Git pull failed in '${my_repo_dir}'."
	else
		mkdir -p "${my_repo_dir%/*}" || die "Can not create parent directory for destination repo at '${my_repo_dir%/*}'."
		git clone "${my_repo_uri}" "${my_repo_dir}" || die "Git clone of '${my_repo_uri}' into '${my_repo_dir}' failed."
	fi
	
}

# If any files differ between indir and outdir, replace the outdir (removing if it exists) with the contents of the indir
write_new_patches_dir() {
	my_patch_indir="${1}" ; shift
	my_patch_outdir="${1:?}" ; shift

	if [ -d "${my_patch_outdir}" ] ; then
		if ! diff -N -q "${my_patch_indir}" "${my_patch_outdir}" ; then
			printf -- "Patches from '${my_patch_indir}' for '${my_patch_outdir}' have been updated.\n"
			printf -- "Removing stale patches directory '${my_patch_outdir}'.\n"
			rm -r "${my_patch_outdir}"
			
		else
			printf -- "Patches directory '${my_patch_outdir}' is up to date.\n"
			return 0
		fi
	fi
	printf -- "Creating patches directory '${my_patch_outdir}' from '${my_patch_indir}.\n"
	mkdir -p "${my_patch_outdir}" || die "Can not create output directory for patches at '${my_patch_outdir}'."
	cp "${my_patch_indir}"/* "${my_patch_outdir}" || die "Failed to copy patches from '${my_patch_indir}/*' to '${my_patch_outdir}'."
}

: ${GENTOO_GCC_REPO_URI:="https://anongit.gentoo.org/git/proj/gcc-patches.git"}
: ${GENTOO_WORK_DIR:="/var/tmp/toolchain/gnu/from-gentoo/work"}
: ${GENTOO_GCC_DIR:="${GENTOO_WORK_DIR}/gcc-patches"}


: ${TOOLCHAIN_OVERLAY_BASEDIR:="/var/git/repo/phkg2-gnu-toolchain"}

: ${GCC_OVERLAY_DIR:="${TOOLCHAIN_OVERLAY_BASEDIR%/}/sys-devel/gcc"}
: ${GCC_OVERLAY_PATCHES_DIR:="${GCC_OVERLAY_DIR%/}/files/patches"}
: ${GENTOO_PATCHES_OUTPUT_BASE:="${GCC_OVERLAY_PATCHES_DIR%/}/gentoo-patches"}

mkdir -p "${GENTOO_WORK_DIR}" || die "Could not create working directiory."
git_clone_or_pull "${GENTOO_GCC_REPO_URI}" "${GENTOO_GCC_DIR}"

for my_patch_gcc_ver in "9.2.0" "8.3.0" "7.4.0" "6.5.0" ; do
	my_patch_indir="${GENTOO_GCC_DIR}/${my_patch_gcc_ver}/gentoo"
	my_patch_ver="$(cat "${my_patch_indir}/README.history" | awk 'NR==1 { print $1; };')"
	my_patch_outdir="${GENTOO_PATCHES_OUTPUT_BASE}/gcc-${my_patch_gcc_ver}-gentoo-${my_patch_ver}"

	write_new_patches_dir "${my_patch_indir}" "${my_patch_outdir}"
done


