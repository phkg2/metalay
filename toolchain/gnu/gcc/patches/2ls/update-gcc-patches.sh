#!/bin/sh
#
my_scriptdir="$(realpath -e ${0})"
my_scriptdir="${my_scriptdir%/*}"


: ${TOOLCHAIN_OVERLAY_BASEDIR:="/var/git/repo/phkg2-gnu-toolchain"}

: ${GCC_OVERLAY_DIR:="${TOOLCHAIN_OVERLAY_BASEDIR%/}/sys-devel/gcc"}
: ${GCC_OVERLAY_PATCHES_DIR:="${TOOLCHAIN_OVERLAY_BASEDIR%/}/sys-devel/gcc/files/patches"}

(printf -- "\nUpdating GCC SVN patches.\n"; sh "${my_scriptdir}/../gcc-svn-update-patches/2ls/update-gcc-svn-update-patches.sh" )
(printf -- "\nUpdating Gentoo GCC patches.\n"; sh "${my_scriptdir}/../gentoo-patches/2ls/update-gentoo-patches.sh" )
(printf -- "\nUpdating Debian GCC patches.\n"; sh "${my_scriptdir}/../debian-patches/2ls/update-debian-patches.sh" )

