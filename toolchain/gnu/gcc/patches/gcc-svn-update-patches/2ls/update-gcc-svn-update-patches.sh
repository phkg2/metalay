#!/bin/sh
#
my_scriptdir="$(realpath -e ${0})"
my_scriptdir="${my_scriptdir%/*}"

: ${TOOLCHAIN_OVERLAY_BASEDIR:="/var/git/repo/phkg2-gnu-toolchain"}

: ${GCC_OVERLAY_DIR:="${TOOLCHAIN_OVERLAY_BASEDIR%/}/sys-devel/gcc"}
: ${GCC_OVERLAY_PATCHES_DIR:="${GCC_OVERLAY_DIR%/}/files/patches"}
: ${GCC_SVN_DIFF_OUT_DIR:="${GCC_OVERLAY_PATCHES_DIR%/}/gcc-svn-updates"}

: ${GCC_SVN_REPO:="http://gcc.gnu.org/svn/gcc"}

sh "${my_scriptdir}/gcc-svn-update-patch-generator.sh" gcc-5-branch gcc_5_5_0_release "${GCC_SVN_DIFF_OUT_DIR}"
sh "${my_scriptdir}/gcc-svn-update-patch-generator.sh" gcc-6-branch gcc_6_5_0_release "${GCC_SVN_DIFF_OUT_DIR}"
sh "${my_scriptdir}/gcc-svn-update-patch-generator.sh" gcc-7-branch gcc_7_4_0_release "${GCC_SVN_DIFF_OUT_DIR}"
sh "${my_scriptdir}/gcc-svn-update-patch-generator.sh" gcc-8-branch gcc_8_3_0_release "${GCC_SVN_DIFF_OUT_DIR}"
sh "${my_scriptdir}/gcc-svn-update-patch-generator.sh" gcc-9-branch gcc_9_2_0_release "${GCC_SVN_DIFF_OUT_DIR}"

