#!/bin/sh

gcc_svn_diff_branch_since_release() {
	my_branch="${1}" ; shift
	my_release_tag="${1}" ; shift
	my_out_dir="$(realpath -e ${1})" ; shift

	my_release="$(printf -- "${my_release_tag}" | sed -re 's/gcc_([0-9]+)_([0-9]+)_([0-9]+)_release/\1.\2.\3/')"
	my_release_rev="$(svn ls -v "${GCC_SVN_REPO}/tags" | grep "${my_release_tag}" | sed -rn 's/[[:space:]]*([[:digit:]]+).*/\1/p')"

	case "${my_branch}" in
		*@[0-9]+)
			echo "${my_release_tag} -> ${my_branch}:"
			my_branch_rev="${my_branch##*@}"
			my_branch="${my_branch%%@*}"
			my_branch_tip=""
		;;
		*)
			echo "${my_release_tag} -> ${my_branch} (tip):"
			my_branch_rev="$(svn ls -v "${GCC_SVN_REPO}/branches" | grep "${my_branch}" | sed -rn 's/[[:space:]]*([[:digit:]]+).*/\1/p')"
			my_branch_tip="tip"
		;;
	esac

	my_patch_out="${my_out_dir}/gcc-${my_release}-to-svn-${my_branch_rev}.patch"


	echo "RELEASE REVISION=${my_release_rev}"
	[ -z "${my_release_rev}" ] && return 1
	echo "BRANCH REVISION=${my_branch_rev}"
	[ -z "${my_branch_rev}" ] && return 1

	if [ ${my_branch_rev} -lt ${my_release_rev} ] ; then
		echo "Release revision ${my_release_rev} is newer than revision ${my_branch_rev} on branch '${my_branch}'. Skipping."
		return 0
	elif [ ${my_branch_rev} -eq ${my_release_rev} ] ; then
		echo "Release revision is up to date with branch '${my_branch}' at revision ${my_branch_rev}. Skipping."
		return 0
	elif [ -f "${my_patch_out}" ] ; then
		echo "Patch '${my_patch_out}' already exists. Skipping."
		return 0
	fi
	printf -- "Generating '${my_patch_out}'..."

	svn diff --git --old "${GCC_SVN_REPO}/tags/${my_release_tag}" --new "${GCC_SVN_REPO}/branches/${my_branch}@${my_branch_rev}" \
		| sed -re 's: (a|b)/(branches|tags)/: \1/:g' \
		> "${my_patch_out}"

	printf -- " Done.\n"
}


if [ $# -ne 3 ] ; then
	printf -- "\nUsage: $0 <branch[@revision]> <base release tag> <output directory>\n"
	if [ "x${1}" = "x--help" ] ; then return 0; else return 1 ; fi
fi

branch="${1}"; shift
base_tag="${1}"; shift
diff_out_dir="${1}"; shift

if ! mkdir -p "${diff_out_dir}" ; then
	printf -- "\nCould not create output directory '%s'.\n" "${diff_out_dir}"
	return 1
fi

: ${GCC_SVN_REPO:="http://gcc.gnu.org/svn/gcc"}
gcc_svn_diff_branch_since_release "${branch}" "${base_tag}" "${diff_out_dir}"



