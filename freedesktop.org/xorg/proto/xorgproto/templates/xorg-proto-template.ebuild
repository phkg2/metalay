# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit meson multilib-minimal

DESCRIPTION="X.Org combined protocol headers"
HOMEPAGE="https://gitlab.freedesktop.org/xorg/proto/xorgproto/"
if [ "${PV}" = "${PV%_p*}" ] ; then
	MY_P="xorgproto-${PV}"
	SRC_URI="https://xorg.freedesktop.org/archive/individual/proto/${MY_P}.tar.gz"
	KEYWORDS="*"
	S="${WORKDIR}/${MY_P}"
else
	inherit git-r3
	EGIT_REPO_URI="https://gitlab.freedesktop.org/xorg/proto/xorgproto"
	PVCD="${PV#*_p}"
	if [ ${PVCD} -ge 19700101 ] && [ ${PVCD} -lt 30000000 ] ; then # Not y3k compliant, sorry!
		PVCDY="${PVCD%????}"
		PVCDMD="${PVCD#????}"
		PVCDM="${PVCDMD%??}"
		PVCDD="${PVCDMD#??}"
		EGIT_COMMIT_DATE="${PVCDY}-${PVCDM}-${PVCDD}"
		KEYWORDS="*"
	else
		# Keyword mask live builds without datestamp.
		KEYWORDS=""
	fi
fi

LICENSE="GPL-2 MIT"
SLOT="0"
IUSE="legacy"


multilib_src_configure() {
	local emesonargs=(
		--datadir="${EPREFIX}/usr/share"
		-Dlegacy=$(usex legacy true false)
	)
	meson_src_configure
}

multilib_src_compile() {
	meson_src_compile
}

multilib_src_install() {
	meson_src_install
}

multilib_src_install_all() {
	DOCS=(
		AUTHORS
		PM_spec
		$(set +f; echo README*)
		$(set +f; echo COPYING-*)
		$(set +f; echo *.txt | grep -v meson*.txt)
	)
	einstalldocs
}

