# Distributed under the terms of the GNU General Public License v2
EAPI=6

DESCRIPTION="X.Org Protocol ${protoname} headers package stub (provided by ${xorgproto_pkg})."

KEYWORDS="*"

SLOT="0"

RDEPEND="=x11-base/${xorgproto_pkg}"
DEPEND="${RDEPEND}"

