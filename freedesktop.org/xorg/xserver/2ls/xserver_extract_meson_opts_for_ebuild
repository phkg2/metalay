#!/bin/env sh
MYSCRIPT="$(realpath -e "${0}")"
MYSCRIPTDIR="${MYSCRIPT%/*}"
[ -z "${METALAY_TOP}" ] && METALAY_TOP="${MYSCRIPTDIR%/*/*/*/*}"
[ -z "${TEMPLATE_DIR}" ] && TEMPLATE_DIR="${MYSCRIPTDIR%/*}/templates"
[ -z "${METADATA_DIR}" ] && METADATA_DIR="${MYSCRIPTDIR%/*}/metadata"

: ${X_OVERLAY_BASEDIR:="/var/git/repo/phkg2-X"}
: ${X_OVERLAY_ECLASS_DIR:="${X_OVERLAY_BASEDIR}/eclass"}
: ${XORG_SERVER_DIR:="${X_OVERLAY_BASEDIR%/}/x11-base/xorg-server"}

die() {
	if [ $# -gt 0 ] ; then
		printf -- "\n%s\n" 1>&2
	else
		printf -- "${0} called die!" 1>&2
	fi
	exit 1
}

IN_FILE="$(realpath "${1:-./meson_options.txt}" )"
[ -d "${IN_FILE}" ] && IN_FILE="${IN_FILE%/}/meson_options.txt"
if ! [ -f "${IN_FILE}" ] ; then
	printf --  "\nCan't find input file '${IN_FILE}'!\n"
	exit 1
fi

# Ignore options that don't make sense here
IGNORE_LIST="platform-sdk-version,osmesa-bits"

# Use DRI based glx by default, including dri3 support
USE_DEFAULT_LIST="+glx_dri,+glx_direct,+dri3"
# Enable support for all GL apis
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+shared-glapi,+opengl,+gles1,+gles2,+egl"
# Enable the gallium based offscreen rendering library (osmesa)
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+osmesa_gallium"
# Use glvnd by default
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+glvnd"
# Enable gallium swrast driver by default
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+gallium-drivers_swrast"
# Add default platforms, including gbm support
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+gbm,+platforms_x11,+platforms_drm,+platforms_surfaceless"
# Use shader cache by default
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+shader-cache"
# Allow vulkan drivers to get xlib lease by default
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+xlib-lease"
# Use LLVM, and link static instead of shared
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},+llvm,+shared-llvm"
# Only enable base-level AVX instructions for SWR by default (enable others manually/in profile)
USE_DEFAULT_LIST="${USE_DEFAULT_LIST},-swr-arches_avx2"


cat "${IN_FILE}" | "${METALAY_TOP}/2ls/buildtools/meson/meson_extract_opts_for_ebuild" -v ignore="${IGNORE_LIST}" -v usedefault="${USE_DEFAULT_LIST}"
