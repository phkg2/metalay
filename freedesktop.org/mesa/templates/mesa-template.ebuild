# Copyright 2019 Chris A. Giorgi, et al.
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{4,5,6,7} )
inherit meson eutils llvm python-any-r1 multilib-minimal

DESCRIPTION="Mesa 3D graphics library"
HOMEPAGE="https://www.mesa3d.org"
EGIT_REPO_URI="https://gitlab.freedesktop.org/mesa/mesa.git"

SRC_URI=""
KEYWORDS=""

case "${PV}" in
	*9999*) : ;;
	*_pre*|*_p*)
		DATE="${PV##*_pre}" ; DATE="${DATE##*_p}"
		EGIT_COMMIT_DATE="${DATE:0:4}-${DATE:4:2}-${DATE:6:2}"
		KEYWORDS="~*"
	;;
	*)
		MY_P="${P/_/-}"
		SRC_URI="https://mesa.freedesktop.org/archive/${MY_P}.tar.xz"
		KEYWORDS="*"
		S="${WORKDIR}/${MY_P}"
	;;
esac

[ -z ${SRC_URI} ] && inherit git-r3

IUSE="debug"

LICENSE="MIT"
SLOT="0"

pkg_setup() {
	llvm_pkg_setup
}

multilib_src_configure() {
	local emesonargs
	set_mesonargs

	emesonargs=(
		${emesonargs[@]}
		--buildtype=$(usex debug debugoptimized release)
		-Db_ndebug=if-release
		--libdir="$(get_libdir)"
		--localstatedir="${EPREFIX}/var/lib"
		--sharedstatedir="${EPREFIX}/usr/share/${P}"
		--prefix="${EPREFIX}/usr"
	)

	if use llvm ; then
		export LLVM_CONFIG="$(get_llvm_prefix)/bin/$(get_abi_CHOST)-llvm-config"
	fi

	meson_src_configure
}

multilib_src_compile() {
	meson_src_compile
}

multilib_src_install() {
	meson_src_install
}

multilib_src_install_all() {
	einstalldocs

	# Cleanup files we shouldn't be installing when using libglvnd
	if use glvnd ; then 
		find "${ED}" -name 'libGLESv[12]*.so*' -delete
		find "${ED}" -name 'gl.pc' -delete
		find "${ED}" -name 'egl.pc' -delete
		if has_version ">=media-libs/libglvnd-1.2.0" ; then
			rm -rf "${ED}/usr/include/{EGL,GL,GLES{,2,3},KHR}"
		fi
	fi
}

multilib_src_test() {
	meson test -v -C "${BUILD_DIR}" -t 100
}


pkg_postinst() {

	if use gallium-omx_bellagio || use gallium-omx_tizonia; then
		echo "XDG_DATA_DIRS=\"${EPREFIX}/usr/share/${P}/xdg\"" > "${T}/99mesaxdgomx"
		doenvd "${T}"/99mesaxdgomx
		keepdir /usr/share/${P}/xdg
	fi

	# run omxregister-bellagio to make the OpenMAX drivers known system-wide
	if use gallium-omx_bellagio ; then
		ebegin "Registering OpenMAX drivers"

		# Populate our search path
		_addpath_omx_bellagio() {
			_BSP="${BSP:+${_BSP}:}${EPREFIX}/usr/$(get_libdir)/${P}/libomxil-bellagio0"
		}
		multilib_foreach_abi _addpath_omx_bellagio

		BELLAGIO_SEARCH_PATH="${_BSP}" \
			OMX_BELLAGIO_REGISTRY=${EPREFIX}/usr/share/${P}/xdg/.omxregister \
			omxregister-bellagio
		eend $?
	fi

	if use gallium-omx_tizonia ; then
		TIZONIA_SEARCH_PATH=""
	fi
}

pkg_prerm() {
	if use gallium-omx_bellagio || use gallium-omx_tizonia ; then
		rm "${EPREFIX}"/usr/share/${P}/xdg/.omxregister
	fi
}

