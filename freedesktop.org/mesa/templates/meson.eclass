
# NOTICE: This is NOT exatly the same as gentoo's meson.eclass!
# (but it should drop in for most ebuilds using the old version)

# Much of this file borrowed in whole or in part from gentoo's meson.eclass
# which is Copyright 2017-2019 Gentoo Authors and distributed under the terms
# of the GNU General Public License v2.
# The remainder is Copyright 2019 Chris A. Giorgi et al.

# @ECLASS: meson.eclass
# @MAINTAINER:
# Chris A. Giorgi <chrisgiorgi@gmail.com>
# @SUPPORTED_EAPIS: 6 7
# @BLURB: common ebuild functions for meson-based packages
# @DESCRIPTION:
# This eclass contains the default phase functions and additional helpers
# for packages which use the meson build system.
#
# @EXAMPLE:
# Typical ebuild using meson.eclass:
#
# @CODE
# EAPI=6
#
# inherit meson
#
# ...
#
# src_configure() {
# 	local emesonargs=(
# 		-Dqt4=$(usex qt4 true false)
# 		-Dthreads=$(usex threads true false)
# 		-Dtiff=$(usex tiff true false)
# 	)
# 	meson_src_configure
# }
#
# ...
#
# @CODE

case ${EAPI:-0} in
	6|7) ;;
	*) die "EAPI=${EAPI} is not supported" ;;
esac

if [[ -z ${_MESON_ECLASS} ]]; then

inherit multiprocessing ninja-utils python-utils-r1 toolchain-funcs

fi

EXPORT_FUNCTIONS src_configure src_compile src_test src_install

if [[ -z ${_MESON_ECLASS} ]]; then
_MESON_ECLASS=1

MESON_DEPEND=">=dev-util/meson-0.51
	>=dev-util/ninja-1.7.2"

if [[ ${EAPI:-0} == [6] ]]; then
	DEPEND="${MESON_DEPEND}"
else
	BDEPEND="${MESON_DEPEND}"
fi

# @ECLASS-VARIABLE: BUILD_DIR
# @DEFAULT_UNSET
# @DESCRIPTION:
# Build directory, location where all generated files should be placed.
# If this isn't set, it defaults to ${WORKDIR}/${P}-build.

# @ECLASS-VARIABLE: EMESON_SOURCE
# @DEFAULT_UNSET
# @DESCRIPTION:
# The location of the source files for the project; this is the source
# directory to pass to meson.
# If this isn't set, it defaults to ${S}

# @VARIABLE: emesonargs
# @DEFAULT_UNSET
# @DESCRIPTION:
# Optional meson arguments as Bash array; this should be defined before
# calling meson_src_configure.

# @VARIABLE: emesontestargs
# @DEFAULT_UNSET
# @DESCRIPTION:
# Optional meson test arguments as Bash array; this should be defined before
# calling meson_src_test.




# From gentoo's meson.eclass as _meson_env_array
# @FUNCTION: string_to_meson_array 
# @USAGE: <strings (from env vars)>
# @DESCRIPTION:
# Parses the command line flags and converts them into an array suitable for
# use in a cross or native environment file.
#
# Input: --single-quote=\' --double-quote=\" --dollar=\$ --backtick=\`
#        --backslash=\\ --full-word-double="Hello World"
#        --full-word-single='Hello World'
#        --full-word-backslash=Hello\ World
#        --simple --unicode-8=C --unicode-16=■ --unicode-32=■
#
# Output: ['--single-quote=\'', '--double-quote="', '--dollar=$',
#          '--backtick=`', '--backslash=\\', '--full-word-double=Hello World',
#          '--full-word-single=Hello World',
#          '--full-word-backslash=Hello World', '--simple', '--unicode-8=C',
#          '--unicode-16=■', '--unicode-32=■']
#
string_to_meson_array() {
read -d '' __MESON_ARRAY_PARSER <<"EOF"
import shlex
import sys

# See http://mesonbuild.com/Syntax.html#strings
def quote(str):
        escaped = str.replace("\\\\", "\\\\\\\\").replace("'", "\\\\'")
        return "'{}'".format(escaped)

print("[{}]".format(
        ", ".join([quote(x) for x in shlex.split(" ".join(sys.argv[1:]))])))
EOF
	python -c "${__MESON_ARRAY_PARSER}" "$@"
}

#
# @FUNCTION: meson_get_system_type
# @USAGE: <system tuple ($CHOST form)>
# @DESCRIPTION:
# Return meson system type for the argument tuple.
#
meson_get_system_type() {
	# List of system types know to meson and their CHOST aliases
	local system="unknown"
	case "${1}" in
		*-cygwin*)			system="cygwin" ;;
		*-darwin*)			system="darwin" ;;
		*-dragonfly*)		system="dragonfly" ;;
		*-emscripten*)		system="emscripten" ;;
		*-freebsd*)			system="freebsd" ;;
		*-haiku*)			system="haiku" ;;
		*-linux*)			system="linux" ;;
		# GNU Hurd, after -linux* to avoid matching -linux-gnu* here
		*-gnu*)			 	system="gnu" ;;
		*-netbsd*)			system="netbsd" ;;
		mingw*|*-mingw*)	system="windows" ;;
		*-solaris*)			system="sunos" ;;
	esac
	printf -- "${system}"
}

meson_get_cpu_family() {
	# List of all cpu familes known to meson and their CHOST aliases
	local family="unknown"
	case "${1}" in
		aarch64*|arm64*)	family="aarch64" ;;
		alpha*)				family="alpha" ;;
		arc*)				family="arc" ;;
		arm*)				family="arm" ;;
		e2k*)				family="e2k" ;;
		ia64*)				family="ia64" ;;
		microblaze*)		family="microblaze" ;;
		mips64*)			family="mips64" ;;
		mips*)				family="mips" ;;
		parisc*|hppa*)		family="parisc" ;;
		ppc64*|powerpc64)	family="ppc64" ;;
		ppc*|powerpc)		family="ppc" ;;
		riscv32*)			family="riscv32" ;;
		riscv64*)			family="riscv64" ;;
		rl78*)				family="rl78" ;;
		rx*)				family="rx" ;;
		s390x*)				family="s390x" ;;
		sparc64*)			family="sparc64" ;;
		sparc*)				family="sparc" ;;
		wasm32*)			family="wasm32" ;;
		wasm64*)			family="wasm64" ;;
		x86_64*|amd64*)		family="x86_64" ;;
		x86*|i*86*)			family="x86" ;;
	esac
	printf -- "${family}"
}



meson_create_buildenv_file() {
	cat > "${T}/mesonenv.${CHOST}.${ABI}" <<-EOF
	[binaries]
	ar = $(string_to_meson_array "$(tc-getAR)")
	c = $(string_to_meson_array "$(tc-getCC)")
	cpp = $(string_to_meson_array "$(tc-getCXX)")
	fortran = $(string_to_meson_array "$(tc-getFC)")
	llvm-config = '${LLVM_CONFIG:-$(tc-getPROG LLVM_CONFIG llvm-config)}'
	objc = $(string_to_meson_array "$(tc-getPROG OBJC cc)")
	objcpp = $(string_to_meson_array "$(tc-getPROG OBJCXX c++)")
	pkgconfig = '$(tc-getPKG_CONFIG)'
	stringip = $(string_to_meson_array "$(tc-getSTRIP)")
	windres = $(string_to_meson_array "$(tc-getRC)")

	[properties]
	c_args = $(string_to_meson_array "${CFLAGS} ${CPPFLAGS}")
	c_link_args = $(string_to_meson_array "${CFLAGS} ${LDFLAGS}")
	cpp_args = $(string_to_meson_array "${CXXFLAGS} ${CPPFLAGS}")
	cpp_link_args = $(string_to_meson_array "${CXXFLAGS} ${LDFLAGS}")
	fortran_args = $(string_to_meson_array "${FCFLAGS}")
	fortran_link_args = $(string_to_meson_array "${FCFLAGS} ${LDFLAGS}")
	objc_args = $(string_to_meson_array "${OBJCFLAGS} ${CPPFLAGS}")
	objc_link_args = $(string_to_meson_array "${OBJCFLAGS} ${LDFLAGS}")
	objcpp_args = $(string_to_meson_array "${OBJCXXFLAGS} ${CPPFLAGS}")
	objcpp_link_args = $(string_to_meson_array "${OBJCXXFLAGS} ${LDFLAGS}")

	[host_machine]
	system = '$(meson_get_system_type "${CHOST}")'
	cpu_family = '$(meson_get_cpu_family "${CHOST}")'
	cpu = '${MCPU:-${CHOST%%-*}}'
	endian = '$(tc-endian)'
	EOF
}


# meson_options_array <option_name> "<useflag_list>" "<choices_list>"
meson_option_array() {
	local myoptname="${1}" ; shift
	local myuflags=( ${1} ) ; shift
	local mychoices=( ${1} ) ; shift

	local n=0
	while [ $n -lt ${#myuflags[*]} ] ; do
		local f="${myuflags[$n]}"
		if use ${f#+} ; then myopts="${myopts:+${myopts},}${mychoices[$n]}" ; fi
		(( n++ ))
	done

	printf -- '-D%s=%s' "${myoptname}" "${myopts}"
}

# meson_options_none_or_combo <option_name> "<default value>" "<useflag_list>" "<choices_list>"
meson_option_none_or_combo() {
	local myoptname="${1}" ; shift
	local mydefault="${1}" ; shift
	local myuflags=( ${1} ) ; shift
	local mychoices=( ${1} ) ; shift

	local n=0
	while [ $n -lt ${#myuflags[*]} ] ; do
		local f="${myuflags[$n]}"
		if use ${f#+} ; then myopts="${mychoices[$n]}" ; fi
		(( n++ ))
	done
	[ -z "${myopts}" ] && myopts="${mydefault}"

	printf -- '-D%s=%s' "${myoptname}" "${myopts}"
}


# meson_option_boolean <option_name> <useflag>
meson_option_boolean() {
	local myoptname="${1}" ; shift
	local myuseflag="${1}" ; shift
	local myopts=""

	if use ${myuseflag} ; then myopts="true" ; else myopts="false" ; fi
	printf -- '-D%s=%s' "${myoptname}" "${myopts}"
}


# meson_option_feature <option_name> <useflag>
meson_option_feature() {
	local myoptname="${1}" ; shift
	local myuseflag="${1}" ; shift
	local myopts=""

	if use ${myuseflag} ; then myopts="enabled" ; else myopts="disabled" ; fi
	printf -- '-D%s=%s' "${myoptname}" "${myopts}"
}


# meson_option_string <option_name> "<string>"
meson_option_string() {
	local myoptname="${1}" ; shift
	local myopts="$*"

	[ -z "${myopts}" ] && return

	printf -- '-D%s=%s' "${myoptname}" "${myopts}"
}


# meson_option_integer <option_name> <integer>
meson_option_integer() {
	local myoptname="${1}" ; shift
	local myopts="${1}" ; shift

	[ -z "${myopts}" ] && return

	printf -- '-D%s=%s' "${myoptname}" "${myopts}"
}


# Backwards compat functions:

# @FUNCTION: meson_use
# @USAGE: <USE flag> [option name]
# @DESCRIPTION:
# Given a USE flag and meson project option, outputs a string like:
#
#   -Doption=true
#   -Doption=false
#
# If the project option is unspecified, it defaults to the USE flag.
meson_use() {
	usex "$1" "-D${2-$1}=true" "-D${2-$1}=false"
}

# @FUNCTION: meson_feature
# @USAGE: <USE flag> [option name]
# @DESCRIPTION:
# Given a USE flag and meson project option, outputs a string like:
#
#   -Doption=enabled
#   -Doption=disabled
#
# If the project option is unspecified, it defaults to the USE flag.
meson_feature() {
	usex "$1" "-D${2-$1}=enabled" "-D${2-$1}=disabled"
}

# @FUNCTION: meson_src_configure
# @USAGE: [extra meson arguments]
# @DESCRIPTION:
# This is the meson_src_configure function.
meson_src_configure() {
	debug-print-function ${FUNCNAME} "$@"

	# Set our build dir early so wee can use it later
	BUILD_DIR="${BUILD_DIR:-${WORKDIR}/${P}-build}"

	# Common args, allow user to override the sensible ones
	local mesonargs=( --wrap-mode nodownload )
	[[ "${emesonargs[*]}" =~ "--buildtype" ]] || mesonargs+=( --buildtype plain )
	[[ "${emesonargs[*]}" =~ "--libdir" ]] || mesonargs+=( --libdir "$(get_libdir)" )
	[[ "${emesonargs[*]}" =~ "--localstatedir" ]] || mesonargs+=( --localstatedir "${EPREFIX}/var/lib" )
	[[ "${emesonargs[*]}" =~ "--prefix" ]] || mesonargs+=( --prefix "${EPREFIX}/usr" )
	[[ "${emesonargs[*]}" =~ "--sysconfdir" ]] || mesonargs+=( --sysconfdir "${EPREFIX}/etc" )

	# Build the toolchain environment file
	meson_create_buildenv_file || die "unable to write meson build environment file"
	if tc-is-cross-compiler ; then
		mesonargs+=( --cross-file "${T}/mesonenv.${CHOST}.${ABI}" )
	else
		mesonargs+=( --native-file "${T}/mesonenv.${CHOST}.${ABI}" )
	fi

	# https://bugs.gentoo.org/625396
	python_export_utf8_locale

	# Append additional arguments from ebuild
	mesonargs+=("${emesonargs[@]}")

	set -- meson "${mesonargs[@]}" "$@" \
		"${EMESON_SOURCE:-${S}}" "${BUILD_DIR}"
	echo "$@"
	tc-env_build "$@" || die
}

# @FUNCTION: meson_src_compile
# @USAGE: [extra ninja arguments]
# @DESCRIPTION:
# This is the meson_src_compile function.
meson_src_compile() {
	debug-print-function ${FUNCNAME} "$@"

	eninja -C "${BUILD_DIR}" "$@"
}

# @FUNCTION: meson_src_test
# @USAGE: [extra meson test arguments]
# @DESCRIPTION:
# This is the meson_src_test function.
meson_src_test() {
	debug-print-function ${FUNCNAME} "$@"

	local mesontestargs=(
		-C "${BUILD_DIR}"
	)
	[[ "${emesontestargs[*]}" =~ "--num-processes" ]] \
		|| [[ -n ${NINJAOPTS} || -n ${MAKEOPTS} ]] \
		&& mesontestargs+=( --num-processes "$(makeopts_jobs ${NINJAOPTS:-${MAKEOPTS}})" )

	# Append additional arguments from ebuild
	mesontestargs+=("${emesontestargs[@]}")

	set -- meson test "${mesontestargs[@]}" "$@"
	echo "$@" >&2
	"$@" || die "tests failed"
}

# @FUNCTION: meson_src_install
# @USAGE: [extra ninja install arguments]
# @DESCRIPTION:
# This is the meson_src_install function.
meson_src_install() {
	debug-print-function ${FUNCNAME} "$@"

	DESTDIR="${D}" eninja -C "${BUILD_DIR}" install "$@"
	einstalldocs
}

fi
